To run the application:

- git clone target
- cd target
- mkdir bin
- javac -sourcepath src -d bin src/com/livestream/Main.java
- java -cp bin com.livestream.Main 5 // 5 being the number of consumers that will be launched
