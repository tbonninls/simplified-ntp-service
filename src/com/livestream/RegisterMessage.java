package com.livestream;

import java.util.concurrent.LinkedBlockingQueue;

public class RegisterMessage extends ConsumerMessage {
  private int id = 0;
  private LinkedBlockingQueue<TimeMessage> timeQueue =
    new LinkedBlockingQueue<TimeMessage>();
  private LinkedBlockingQueue<KeepAliveMessage> keepAliveQueue =
    new LinkedBlockingQueue<KeepAliveMessage>();
  
  public RegisterMessage(int id) {
    this.id = id;
  }
  
  public int getId() {
    return id;
  }
  
  public LinkedBlockingQueue<TimeMessage> getTimeQueue() {
    return timeQueue;
  }
  
  public LinkedBlockingQueue<KeepAliveMessage> getKeepAliveQueue() {
    return keepAliveQueue;
  }
  
}