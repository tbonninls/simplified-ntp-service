package com.livestream;

import java.sql.Date;
import java.util.concurrent.LinkedBlockingQueue;

public class ConsumerData {
  private RegisterMessage message = null;
  private Date lastKeepAlive = null;
  
  public ConsumerData(RegisterMessage message) {
    this.message = message;
    this.lastKeepAlive = new Date(System.currentTimeMillis());
  }
  
  public int getId() {
    return message.getId();
  }
  
  public LinkedBlockingQueue<TimeMessage> getTimeQueue() {
    return message.getTimeQueue();
  }
  
  public LinkedBlockingQueue<KeepAliveMessage> getKeepAliveQueue() {
    return message.getKeepAliveQueue();
  }
  
  public Date getLastKeepAlive() {
    return lastKeepAlive;
  }
  
  public void updateLastKeepAlive() {
    this.lastKeepAlive = new Date(System.currentTimeMillis());
  }
  
}
