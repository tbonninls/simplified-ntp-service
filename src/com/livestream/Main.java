package com.livestream;

public class Main {
  private static int MIN = 0;
  private static int MAX = 12;
  
  private static int generateRandomMaxKeepAliveMessageCount() {
    return MIN + (int)(Math.random() * ((MAX - MIN) + 1));
  }
  
  public static void main(String[] args) {
    int consumersCount = Integer.parseInt(args[0]) + 1;
    
    Producer producer = new Producer();
    producer.start();
    
    for (int i = 1; i < consumersCount; i++) {
      int maxKeepAliveMessages = generateRandomMaxKeepAliveMessageCount();
      Consumer consumer = new Consumer(i, maxKeepAliveMessages);
      consumer.start();
    }
  }

}
