package com.livestream;

import java.sql.Date;

public class TimeMessage extends ProducerMessage {
  private Date time = new Date(System.currentTimeMillis());
  
  public Date getTime() {
    return time;
  }
  
}