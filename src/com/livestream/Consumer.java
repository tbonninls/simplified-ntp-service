package com.livestream;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class Consumer extends Thread {
  private DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
  private int keepAliveMessagesSent = 0;
  
  private int maxKeepAliveMessages = 0;
  private RegisterMessage registerMessage = null;
  private long lastKeepAliveTimeSent = 0;
  private boolean shouldRun = true;
  
  public Consumer(int id, int maxKeepAliveMessages) {
    this.maxKeepAliveMessages = maxKeepAliveMessages;
    this.registerMessage = new RegisterMessage(id);
    this.setName("consumer-" + id);
  }
  
  private void println(String message) {
    System.out.println("[" + getName() + "]: " + message);
  }
  
  @Override
  public void run() {
    println("Started with maxKeepAliveMessages=" + maxKeepAliveMessages);
    this.lastKeepAliveTimeSent = System.currentTimeMillis();
    Producer.registerQueue.offer(registerMessage);
    while (shouldRun) {
      long now = System.currentTimeMillis();
      if (keepAliveMessagesSent < maxKeepAliveMessages && now - lastKeepAliveTimeSent >= 5000) {
        registerMessage.getKeepAliveQueue().offer(new KeepAliveMessage());
        keepAliveMessagesSent++;
        lastKeepAliveTimeSent = System.currentTimeMillis();
      }
      TimeMessage timeMessage = registerMessage.getTimeQueue().poll();
      if (timeMessage != null) {
        println(format.format(timeMessage.getTime()));
      }
      try {
        Thread.sleep(100);
      } catch(InterruptedException e) {
        
      }
    }
    println("Shutting down");
  }
  
  public void shutdown() {
    shouldRun = false;
  }
  
}
