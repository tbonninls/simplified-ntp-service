package com.livestream;

import java.util.ArrayList;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.Date;

public class Producer extends Thread {
  private boolean shouldRun = true;
  private ArrayList<ConsumerData> consumers = new ArrayList<ConsumerData>();
  public static LinkedBlockingQueue<RegisterMessage> registerQueue =
    new LinkedBlockingQueue<RegisterMessage>();
  
  
  public Producer() {
    this.setName("producer");
  }
  
  private void println(String message) {
    System.out.println("[" + getName() + "]: " + message);
  }
  
  private String getConsumerName(ConsumerData consumer) {
    return "[consumer-" + consumer.getId() + "]";
  }
  
  @Override
  public void run() {
    println("Started");
    while (shouldRun) {
      RegisterMessage message = null;
      try {
        message = registerQueue.poll(1, TimeUnit.SECONDS);
      } catch(InterruptedException e) {
        
      }
      if (message != null) {
        consumers.add(new ConsumerData(message));
      }
      
      Date tenSecondsEarlier = new Date(System.currentTimeMillis() - 10000);
      TimeMessage timeMessage = new TimeMessage();
      int size = consumers.size();
      for (int i = 0; i < size; i++) {
        ConsumerData consumer = consumers.get(i);
        KeepAliveMessage keepAliveMessage = consumer.getKeepAliveQueue().poll();
        if (keepAliveMessage != null) {
          println("Received KeepAlive from " + getConsumerName(consumer));
          consumer.updateLastKeepAlive();
        } else if (consumer.getLastKeepAlive().before(tenSecondsEarlier)) {
          println("Removing " + getConsumerName(consumer));
          consumers.remove(i);
          size--;
          i--;
          continue;
        }
        println("Sending Time message to " + getConsumerName(consumer));
        consumer.getTimeQueue().offer(timeMessage);
      }
    }
    println("Shutting down");
  }
  
  public void shutdown() {
    shouldRun = false;
  }
  
}
